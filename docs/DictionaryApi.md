# lingvo_client.DictionaryApi

All URIs are relative to *https://developers.lingvolive.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_v1_sound_get**](DictionaryApi.md#api_v1_sound_get) | **GET** /api/v1/Sound | Sound
[**api_v1_translation_get**](DictionaryApi.md#api_v1_translation_get) | **GET** /api/v1/Translation | Translation


# **api_v1_sound_get**
> str api_v1_sound_get()

Sound

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import lingvo_client
from lingvo_client.api import dictionary_api
from pprint import pprint
# Defining the host is optional and defaults to https://developers.lingvolive.com
# See configuration.py for a list of all supported configuration parameters.
configuration = lingvo_client.Configuration(
    host = "https://developers.lingvolive.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = lingvo_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with lingvo_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dictionary_api.DictionaryApi(api_client)
    dictionary_name = "LingvoUniversal (En-Ru)" # str |  (optional)
    file_name = "mother.wav" # str |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Sound
        api_response = api_instance.api_v1_sound_get(dictionary_name=dictionary_name, file_name=file_name)
        pprint(api_response)
    except lingvo_client.ApiException as e:
        print("Exception when calling DictionaryApi->api_v1_sound_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dictionary_name** | **str**|  | [optional]
 **file_name** | **str**|  | [optional]

### Return type

**str**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_v1_translation_get**
> [ArticleModel] api_v1_translation_get()

Translation

Словарный перевод слова/фразы. Поиск осуществляется только в указанном направлении

### Example

* Bearer Authentication (BearerAuth):

```python
import time
import lingvo_client
from lingvo_client.api import dictionary_api
from lingvo_client.model.article_model import ArticleModel
from pprint import pprint
# Defining the host is optional and defaults to https://developers.lingvolive.com
# See configuration.py for a list of all supported configuration parameters.
configuration = lingvo_client.Configuration(
    host = "https://developers.lingvolive.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: BearerAuth
configuration = lingvo_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)

# Enter a context with an instance of the API client
with lingvo_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = dictionary_api.DictionaryApi(api_client)
    text = "go" # str |  (optional)
    src_lang = 1033 # int |  (optional)
    dst_lang = 1049 # int |  (optional)

    # example passing only required values which don't have defaults set
    # and optional values
    try:
        # Translation
        api_response = api_instance.api_v1_translation_get(text=text, src_lang=src_lang, dst_lang=dst_lang)
        pprint(api_response)
    except lingvo_client.ApiException as e:
        print("Exception when calling DictionaryApi->api_v1_translation_get: %s\n" % e)
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **text** | **str**|  | [optional]
 **src_lang** | **int**|  | [optional]
 **dst_lang** | **int**|  | [optional]

### Return type

[**[ArticleModel]**](ArticleModel.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

