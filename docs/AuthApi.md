# lingvo_client.AuthApi

All URIs are relative to *https://developers.lingvolive.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_v1_authenticate_post**](AuthApi.md#api_v1_authenticate_post) | **POST** /api/v1/authenticate | Auth


# **api_v1_authenticate_post**
> str api_v1_authenticate_post()

Auth

### Example

* Basic Authentication (BasicAuth):

```python
import time
import lingvo_client
from lingvo_client.api import auth_api
from pprint import pprint
# Defining the host is optional and defaults to https://developers.lingvolive.com
# See configuration.py for a list of all supported configuration parameters.
configuration = lingvo_client.Configuration(
    host = "https://developers.lingvolive.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: BasicAuth
configuration = lingvo_client.Configuration(
    username = 'YOUR_USERNAME',
    password = 'YOUR_PASSWORD'
)

# Enter a context with an instance of the API client
with lingvo_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = auth_api.AuthApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Auth
        api_response = api_instance.api_v1_authenticate_post()
        pprint(api_response)
    except lingvo_client.ApiException as e:
        print("Exception when calling AuthApi->api_v1_authenticate_post: %s\n" % e)
```


### Parameters
This endpoint does not need any parameter.

### Return type

**str**

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: text/plain


### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

