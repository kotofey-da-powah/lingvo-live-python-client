#!/bin/zsh

openapi-generator-cli generate \
  -i spec.yaml \
  -g python \
  --additional-properties=packageName=lingvo_client